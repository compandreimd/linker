﻿using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Linker.Repository;
using Linker.Services;
using FluentAssertions;
using Moq;
namespace UnitTestProject1
{
    [TestClass]
    public class LayerTest
    {

        [TestMethod]
        public void LayerGetCookie()
        {
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            ILayer layer = new Layer(MDB.Object);
            //For NewOne
            Mock<HttpRequestBase> MReq = new Mock<HttpRequestBase>();
            HttpCookieCollection collection = new HttpCookieCollection();
            MReq.Setup(req => req.Cookies).Returns(collection);
            Mock<HttpResponseBase> MRes = new Mock<HttpResponseBase>();
            MRes.Setup(res => res.Cookies).Returns(collection);
            layer.GetCookie(MReq.Object, MRes.Object);
            collection["UserId"].Should().NotBeNull();
            //For OldOne
            MReq = new Mock<HttpRequestBase>();
            collection = new HttpCookieCollection();
            string value = "C13DF";
            collection.Add(new HttpCookie("UserId", value));
            MReq.Setup(req => req.Cookies).Returns(collection);
            MRes = new Mock<HttpResponseBase>();
            MRes.Setup(res => res.Cookies).Returns(collection);
            layer.GetCookie(MReq.Object, MRes.Object);
            collection["UserId"].Value.ShouldBeEquivalentTo(value);
        }

        [TestMethod]
        public void LayerGetUsername()
        {
            //No Username
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            ILayer layer = new Layer(MDB.Object);
            layer.GetUsername().Should().BeNull();
            
            //Existing Username

            Mock<HttpRequestBase> MReq = new Mock<HttpRequestBase>();
            HttpCookieCollection collection = new HttpCookieCollection();
            MReq.Setup(req => req.Cookies).Returns(collection);
            Mock<HttpResponseBase> MRes = new Mock<HttpResponseBase>();
            MRes.Setup(res => res.Cookies).Returns(collection);
            layer.GetCookie(MReq.Object, MRes.Object) ;
            MDB.Setup(db => db.GetUserByCookie(collection["UserId"].Value)).Returns("User");
            layer.GetUsername().ShouldBeEquivalentTo("User");
        }

    }
}
