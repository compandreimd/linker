﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web;
using Linker.Repository;
using Linker.Services;
using FluentAssertions;
using System.Collections.Generic;
using System.Web.Routing;

namespace UnitTestProject1
{
    [TestClass]
    public class MainLayerTest
    {
        public MainLayerTest()
        { 
            
            HttpContext.Current = new HttpContext(new HttpRequest("/","http://localhost:2321","/"), new HttpResponse(null));
        }

        [TestMethod]
        public void MainLayerGetLinks()
        {
            //Empty
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            List<Link> list = new List<Link>();
            MDB.Setup(db => db.GetListBy(null)).Returns(list);
            IMainLayer layer = new MainLayer(MDB.Object);
            layer.GetLinks().Should().BeEmpty();
           
            //Has element
            list.Add(new Link() { LinkId = 128723, By = "X", Count = 10, LongURL = "http://google.com" });
            var nlist = layer.GetLinks();
            nlist.Count.ShouldBeEquivalentTo(1);
            nlist[0].LongURL.ShouldBeEquivalentTo(list[0].LongURL);
            nlist[0].Count.ShouldBeEquivalentTo(list[0].Count);
            nlist[0].ShortURL.ShouldBeEquivalentTo("http://localhost:2321/TyW");
        }

        [TestMethod]
        public void MainLayerOnRedirect()
        {
            RouteData rd = new RouteData();
            rd.Values.Add("id", "TyW");
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            List<Link> list = new List<Link>();
            MDB.Setup(db => db.GetListById(128723)).Returns(list);
            IMainLayer layer = new MainLayer(MDB.Object);
            Mock<HttpRequestBase> MR = new Mock<HttpRequestBase>();
            Mock<HttpResponseBase> MS = new Mock<HttpResponseBase>();
            Int32 code = 200;
            String desc = "";

            MS.SetupSet(m => m.StatusCode = code).Callback<Int32>(m => code = m);
            MS.SetupSet(m => m.StatusDescription = desc).Callback<String>(s => desc = s) ;
            layer.OnRedirect(rd, MR.Object, MS.Object);
            MS.VerifySet(m => m.StatusCode = 404);
            MS.VerifySet(m => m.StatusDescription = "Page not found ");
            list.Add(new Link() { LinkId = 128723, By = "X", Count = 10, LongURL = "http://google.com" });
            layer.OnRedirect(rd, MR.Object, MS.Object);
            MS.Verify(m => m.Redirect("http://google.com"));
        }

        [TestMethod]
        public void MainLayerOnView()
        {
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            MainLayer layer = new MainLayer(MDB.Object);
            //Test on index without cookie
            RouteData rd = new RouteData();
            RouteValueDictionary dic = new RouteValueDictionary();
            Mock<HttpRequestBase> MR = new Mock<HttpRequestBase>();
            Mock<HttpResponseBase> MS = new Mock<HttpResponseBase>();
            var oldcookie = new HttpCookieCollection();
            var newcookie = new HttpCookieCollection();
            var forms = new System.Collections.Specialized.NameValueCollection();
            MR.Setup(m => m.Cookies).Returns(oldcookie);
            MR.Setup(m => m.Form).Returns(forms);
            MS.Setup(m => m.Cookies).Returns(newcookie);
            layer.OnView(rd, MR.Object, MS.Object).ShouldBeEquivalentTo(true);
            newcookie["UserId"].Should().NotBeNull();
            oldcookie.Add(new HttpCookie("UserId", "UID"));
            layer.OnView(rd, MR.Object, MS.Object).ShouldBeEquivalentTo(true);
            rd.Values.Add("id", "2hsdisd");
            layer.OnView(rd, MR.Object, MS.Object).ShouldBeEquivalentTo(false);
        }
    }
}
