﻿using System;
using Linker.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace UnitTestProject1
{
    [TestClass]
    public class DataBaseTest
    {
        Random r = new Random();
        IDataBase DB = new DataBase(true);

        [TestMethod]
        public void DataBaseCreateDataBase()
        {
            
            DB.Should().NotBeNull();
        }

        [TestMethod]
        public void DataBaseAddLink()
        {
            DB.AddLink("http://google.com/", "Cookie", 10);
            var list = DB.GetListBy("Cookie");
            list.Should().NotBeNullOrEmpty();
            list.Count.ShouldBeEquivalentTo(1);
            list[0].LongURL.ShouldBeEquivalentTo("http://google.com/");
            list[0].By.ShouldBeEquivalentTo("Cookie");
            list[0].Count.ShouldBeEquivalentTo(10);
        }

        [TestMethod]
        public void DataBaseAddUser()
        {
            DB.AddUser("Admin", r.Next().ToString(), "Cookie");
            DB.ExistUser("Admin").Should().BeTrue();
        }

        [TestMethod]
        public void DataBaseChangesCookie()
        {
            DB.AddUser("AdminCC", r.Next().ToString(), "CC");
            DB.AddLink("http://google.com/", "CC", 0);
            DB.ChangesCookie("CC", "CC2");
            DB.GetUserByCookie("CC2").ShouldAllBeEquivalentTo("AdminCC");
            var list = DB.GetListBy("CC2");
            list.Should().NotBeNullOrEmpty();
            list.Count.ShouldBeEquivalentTo(1);
            list[0].LongURL.ShouldBeEquivalentTo("http://google.com/");
            list[0].By.ShouldBeEquivalentTo("CC2");
            list[0].Count.ShouldBeEquivalentTo(0);
        }

        [TestMethod]
        public void DataBaseExistUser()
        {
            DB.AddUser("EAdmin", r.Next().ToString(), "EC");
            DB.ExistUser("EAdmin").ShouldBeEquivalentTo(true);
            DB.ExistUser("EAdmin2").ShouldBeEquivalentTo(false);
        }
        
        [TestMethod]
        public void DataBaseGetListBy()
        {
            DB.AddLink("http://google.com/", "GLB", 10);
            DB.AddLink("http://yahoo.com/", "GLB", 5);
            var list = DB.GetListBy("GLB");
            list.Should().NotBeNullOrEmpty();
            list.Count.ShouldBeEquivalentTo(2);
            list[0].LongURL.ShouldBeEquivalentTo("http://google.com/");
            list[0].By.ShouldBeEquivalentTo("GLB");
            list[0].Count.ShouldBeEquivalentTo(10);
            list[1].LongURL.ShouldBeEquivalentTo("http://yahoo.com/");
            list[1].By.ShouldBeEquivalentTo("GLB");
            list[1].Count.ShouldBeEquivalentTo(5);
        }
        [TestMethod]
        public void DataBaseGetListById()
        {
            DataBaseAddLink();
            DB.GetListById(1000).Should().BeNullOrEmpty();
            var list = DB.GetListById(1);
            list.Should().NotBeNullOrEmpty();
            list[0].LongURL.ShouldBeEquivalentTo("http://google.com/");
            list[0].By.ShouldBeEquivalentTo("Cookie");
            list[0].Count.ShouldBeEquivalentTo(10);
        }
        [TestMethod]
        public void DataBaseGetUserByCookie()
        {
            DB.AddUser("UserBC", r.Next().ToString(), "GUBC");
            DB.GetUserByCookie("GUBC").ShouldBeEquivalentTo("UserBC");
        }
        [TestMethod]
        public void DataBaseGetUserCookie()
        {
            DB.AddUser("UserC", r.Next().ToString(), "GUC");
            DB.GetUserCookie("UserC").ShouldBeEquivalentTo("GUC");
        }
        [TestMethod]
        public void DataBaseGetUserPassword()
        {
            string pass = r.Next().ToString();
            DB.AddUser("UserBP", pass, "GUP");
            DB.GetUserPassword("UserBP").ShouldBeEquivalentTo(pass);
        }


        public void DataBaseSave()
        {
            //ToDo Check Saving Datasave
        }
    }
}
