﻿using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Linker.Services;
using Moq;
using Linker.Repository;
using FluentAssertions;
using System;
using System.Collections.Specialized;

namespace UnitTestProject1
{
    [TestClass]
    public class UserLayerTest
    {
        [TestMethod]
        public void UserLayerOnLogOut()
        {
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            IUserLayer layer = new UserLayer(MDB.Object);
            Mock<HttpResponseBase> MRes = new Mock<HttpResponseBase>();
            var newcookie = new HttpCookieCollection();
            MRes.Setup(m => m.Cookies).Returns(newcookie);
            Mock<HttpSessionStateBase> MSession = new Mock<HttpSessionStateBase>();
            layer.OnLogOut(MRes.Object, MSession.Object);
            MSession.Verify(m => m.Abandon());
            MRes.Verify(m => m.Redirect("/"));
            newcookie.Add(new HttpCookie("UserId", "w1022"));
            layer.OnLogOut(MRes.Object, MSession.Object);
            newcookie["UserId"].Expires.ShouldBeEquivalentTo(DateTime.Now.AddDays(-1));
            MSession.Verify(m => m.Abandon());
            MRes.Verify(m => m.Redirect("/"));
        }

        [TestMethod]
        public void UserLayerOnRegister()
        {
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            MDB.Setup(m => m.ExistUser("ccc")).Returns(true);
            MDB.Setup(m => m.ExistUser("cccp")).Returns(false);
            IUserLayer layer = new UserLayer(MDB.Object);
            var cookie = new HttpCookieCollection();
            Mock<HttpResponseBase> MRes = new Mock<HttpResponseBase>();
            Mock<HttpRequestBase> MReq = new Mock<HttpRequestBase>();
            NameValueCollection form = new NameValueCollection();
            MReq.Setup(e => e.Cookies).Returns(cookie);
            MReq.Setup(e => e.Form).Returns(form);
            MRes.Setup(e => e.Cookies).Returns(cookie);
            layer.OnRegister(MReq.Object, MRes.Object).Should().BeNull();
            form["username"] = "cccp";
            layer.OnRegister(MReq.Object, MRes.Object).Should().BeNull();
            form["password"] = "14325";
            layer.OnRegister(MReq.Object, MRes.Object).ShouldBeEquivalentTo("Passwords are different");
            form["repeat"] = "14325";
            layer.OnRegister(MReq.Object, MRes.Object).Should().BeNull();
            MRes.Verify(r => r.Redirect("/"));
            form["username"] = "ccc";
            layer.OnRegister(MReq.Object, MRes.Object).ShouldBeEquivalentTo("Exist Such User");
        }

        [TestMethod]
        public void UserLayerOnSignIn()
        {
            Mock<IDataBase> MDB = new Mock<IDataBase>();
            MDB.Setup(m => m.GetUserPassword("cccp")).Returns("pass");
            MDB.Setup(m => m.GetUserCookie("cccp")).Returns("URS");
            IUserLayer layer = new UserLayer(MDB.Object);
            var cookie = new HttpCookieCollection();
            Mock<HttpResponseBase> MRes = new Mock<HttpResponseBase>();
            Mock<HttpRequestBase> MReq = new Mock<HttpRequestBase>();
            NameValueCollection form = new NameValueCollection();
            MReq.Setup(e => e.Cookies).Returns(cookie);
            MReq.Setup(e => e.Form).Returns(form);
            MRes.Setup(e => e.Cookies).Returns(cookie);
            layer.OnSignIn(MReq.Object, MRes.Object).Should().BeNull();
            form["username"] = "dsidu";
            form["password"] = "dsidu";
            layer.OnSignIn(MReq.Object, MRes.Object).ShouldBeEquivalentTo("No such Username!");
            form["username"] = "cccp";
            layer.OnSignIn(MReq.Object, MRes.Object).ShouldBeEquivalentTo("Wrong Password!");
            form["password"] = "pass";
            layer.OnSignIn(MReq.Object, MRes.Object).Should().BeNull();
            MRes.Verify(r => r.Redirect("/"));
        }

    }
}
