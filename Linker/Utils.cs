﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Linker
{
    public class Utils
    {
        public static string StrBase = "1qazQAZ2wsxWSX3edcEDC4rfvRFV5tgbTGB6yhnYHN7ujmUJM8ikIKol0pOLP9";

        public static string Int2Base(int Nr)
        {
            string str = "";
            int b = StrBase.Length;
            while (Nr != 0)
            {
                str = StrBase[Nr % b] + str;
                Nr /= b;
            }
            return str;
        }
        public static int Base2Int(string Str)
        {
            int nr = 0;
            int b = StrBase.Length;
            for (int i = 0; i < Str.Length; i++)
            {
                nr *= b;
                int k = StrBase.IndexOf(Str[i]);
                if (k < 0)
                    return -1;
                nr += k;
            }
            return nr;
        }

        public static string ShortLink(int LinkId)
        {
            return "http://"+HttpContext.Current.Request.Url.Host+":"
                +HttpContext.Current.Request.Url.Port+"/"+
                Int2Base(LinkId);
        }
    }
}