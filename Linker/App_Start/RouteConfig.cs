﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Linker
{
    public class RouteConfig
    {
    

        public static void RegisterRoutes(RouteCollection routes)
        { 
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Default",
              url: "{id}",
              defaults: new { controller = "Linker", action = "Index", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                name: "UserRoute",
                url: "User/{action}",
                defaults: new { controller = "User", action = "SingIn", id = "" }
                );

        }
    }
}
