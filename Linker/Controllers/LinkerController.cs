﻿using System;
using System.Web.Mvc;
using Linker.Services;
using Linker.Models;

namespace Linker.Controllers
{

    public class LinkerController : Controller
    {
        IMainLayer ServicesLayer = new MainLayer();

        [Route("{id}")]
        public ActionResult Index(String str)
        {
            if (ServicesLayer.OnView(RouteData, Request, Response))
            {
                LinkerModel model = new LinkerModel()
                {
                    Links = ServicesLayer.GetLinks(),
                    Username = ServicesLayer.GetUsername()
                };
                return PartialView(model);
            }
            ServicesLayer.OnRedirect(RouteData, Request, Response);
            return null;
        }
    }
}
