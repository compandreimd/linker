﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Linker.Models;
using Linker.Services;

namespace Linker.Controllers
{
    public class UserController : Controller
    {
        IUserLayer ServicesLayer = new UserLayer();
        public ActionResult SingIn()
        {
            InfoModel info = new InfoModel()
            {
                Info = ServicesLayer.OnSignIn(Request, Response),
                Username = ServicesLayer.GetUsername()
            };
            return PartialView(info);
        }

        public ViewResult LogOut()
        {
            ServicesLayer.OnLogOut(Response, Session);
            return null;
        }

        public ActionResult Register()
        {
            InfoModel info = new InfoModel()
            {
                Info = ServicesLayer.OnRegister(Request, Response),
                Username = ServicesLayer.GetUsername()
            };
            
            return PartialView(info);
            
        }
    }
}
