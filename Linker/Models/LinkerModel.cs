﻿using System.Collections.Generic;
using System;
using Linker.Services;

namespace Linker.Models
{
    public class LinkerModel
    {
        public String Username { get; set; }
        public List<LinkView> Links { get; set; }
    }
}