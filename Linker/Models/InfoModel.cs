﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Linker.Models
{
    public class InfoModel
    {
        public String Username { get; set; }
        public String Info { get; set; }
    }
}