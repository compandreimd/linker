﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Linker.Models
{
    public class UserDB
    {

        public int UserId { get; set; }
    
        public String UserName { get; set; }

        public String Password { get; set; }
        [Required]
        public String LastCookie { get; set; }
    }
}