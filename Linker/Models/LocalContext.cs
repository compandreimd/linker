﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Linker.Models
{
    public class LocalContext : DbContext
    {
        
        public DbSet<LinkDB> Links { get; set; }
        public DbSet<UserDB> Users { get; set; }
    }
}