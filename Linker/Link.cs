﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Linker
{
    public class Link
    {
        public int LinkId;
        public String LongURL;
        public String ShortURL;
        public String By;
        public int Count;

        /*Static*/
        public static List<Link> AllLinks = new List<Link>();

        /*Static*/

        public static List<Link> UsersLinks(String UserId)
        {
            return AllLinks.Where(item => item.By == UserId).ToList();
        }

        /*Static Method*/

        public static Link Find(string str)
        {
            for(int i =0; i < AllLinks.Count; i++)
            {
                   if(AllLinks[i].ShortURL == str)
                     {
                    return AllLinks[i];
                    }
            }
            return null;
        }

        /*Methods*/


        public Link(String Long, String By)
        {
            //this.LinkId = AllLinks.Count;
            this.LongURL = Long;
            this.ShortURL = ""+AllLinks.Count;
            this.By = By;
            AllLinks.Add(this);
        }

        ~Link()
        {
            AllLinks.Remove(this);
        }
    }
}