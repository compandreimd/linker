﻿using Linker.Repository;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
namespace Linker.Services
{
    public class MainLayer : Layer, IMainLayer
    {
        private List<LinkView> LinksView = new List<LinkView>(); 

        public MainLayer() {}

        public MainLayer(IDataBase DB) : base(DB) {}

        void OnPost(HttpRequestBase Request)
        {
            if (Request.Form["link"] != null)
            {
                DB.AddLink(Request.Form["link"],LastCookies, 0);
            }
        }

        string ShortLink(int LinkId)
        {
            return "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/" + Int2Base(LinkId);
        }

        void AddLinkToLinkView(Link link)
        {
            LinksView.Add(new LinkView {
                LongURL = link.LongURL,
                Count = link.Count,
                ShortURL = ShortLink(link.LinkId)
            });
        }

        public bool OnView(RouteData RouteData, HttpRequestBase Request, HttpResponseBase Response)
        {
            if (RouteData.Values["id"] == null)
            {
                GetCookie(Request, Response);
                OnPost(Request);
                return true;
            }
            return false;
        }

        public void OnRedirect(RouteData RouteData, HttpRequestBase Request, HttpResponseBase Response)
        {
            int LinkId = Base2Int(RouteData.Values["id"].ToString());
            List<Link> Links = DB.GetListById(LinkId);
            if (Links.Count <= 0)
            {
                //No Links return 404
                Response.StatusCode = 404;
                Response.StatusDescription = "Page not found ";
                return;
            }
            Link link = Links[0];
            link.Count++;
            DB.Save();
            Response.Redirect(link.LongURL);
        }

        public List<LinkView> GetLinks()
        {
            List<Link> list = DB.GetListBy(LastCookies);
            list.ForEach(AddLinkToLinkView);
            return LinksView;
        }

    }
}
