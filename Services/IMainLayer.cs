﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Linker.Services
{
    public interface IMainLayer : ILayer
    {
        List<LinkView> GetLinks();
        void OnRedirect(System.Web.Routing.RouteData RouteData, System.Web.HttpRequestBase Request, System.Web.HttpResponseBase Response);
        bool OnView(System.Web.Routing.RouteData RouteData, System.Web.HttpRequestBase Request, System.Web.HttpResponseBase Response);
    }
}