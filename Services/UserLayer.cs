﻿using System;
using System.Web;
using Linker.Repository;

namespace Linker.Services
{
    public class UserLayer :Layer, IUserLayer
    {
        public UserLayer()
        {

        }
        public UserLayer(IDataBase DB):base(DB)
        {

        }
        public void OnLogOut(HttpResponseBase Response, HttpSessionStateBase Session)
        {
            if (Response.Cookies["UserId"] != null)
            {
                Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(-1);
            }
            Session.Abandon();
            Response.Redirect("/");
        }
        String OnPostRegister(HttpRequestBase Request, HttpResponseBase Response)
        {
            if (Request.Form["username"] != null && Request.Form["password"] != null)
            {
                if (LastCookies == null)
                    return "No Cookie";
                if (Request.Form["password"] != Request.Form["repeat"])
                    return "Passwords are different";
                string Username = Request.Form["username"].ToString();
                if (DB.ExistUser(Username))
                    return "Exist Such User";
                DB.AddUser(Username, Request.Form["password"], LastCookies);
                Response.Redirect("/");
                return null;
            }
            return null;
        }
        public String OnRegister(HttpRequestBase Request, HttpResponseBase Response)
        {
            GetCookie(Request,Response);
            return OnPostRegister(Request, Response);
        }

        String OnPostSI(HttpRequestBase Request, HttpResponseBase Response)
        {
            if (Request.Form["username"] != null && Request.Form["password"] != null)
            {
                string username = Request.Form["username"];
                String pass = DB.GetUserPassword(username);
                if (pass == null)
                    return "No such Username!";
                if (pass != Request.Form["password"])
                    return "Wrong Password!";
                var cookie = DB.GetUserCookie(username);
                DB.ChangesCookie(cookie, LastCookies);
                Response.Redirect("/");
            }
            return null; 
        }

        public String OnSignIn(HttpRequestBase Request, HttpResponseBase Response)
        {
            GetCookie(Request, Response);
            return OnPostSI(Request, Response);
        }
    }
}
