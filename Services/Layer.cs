﻿using System;
using System.Web;
using Linker.Repository;
namespace Linker.Services
{
    public class Layer : ILayer
    {
        string StrBase = "1qazZAQ2wsxXSW3edcCDE4rfvVFR5tgbBGT6yhnNHY7ujmMJU8ikKI9olLO0p";
        int LenBase = 0;
        protected string LastCookies = null;
        protected IDataBase DB = new DataBase();
        public Layer()
        {
            LenBase = StrBase.Length;
            DB = new DataBase();
        }

        public Layer(IDataBase DB)
        {
            LenBase = StrBase.Length;
            this.DB = DB;
        }
        public void GetCookie(HttpRequestBase Request, HttpResponseBase Response)
        {
            if (Request.Cookies["UserId"] == null)
            {
                //Random Genereting UserId
                Random rand = new Random();
                //Create Cookie 
                HttpCookie cookie = new HttpCookie("UserId");
                cookie.Value = Int2Base(rand.Next());
                //Store Value to using foward
                if (Response != null)
                    Response.Cookies.Add(cookie);
                LastCookies = cookie.Value;
            }
            else
                LastCookies = Request.Cookies["UserId"].Value;
        }
        public String GetUsername()
        {
            return DB.GetUserByCookie(LastCookies);
        }

        protected string Int2Base(int Int)
        {
            string str = "";

            while (Int > 0)
            {
                str = StrBase[Int % LenBase] + str;
                Int /= LenBase;
            }
            return str;
        }
        protected int Base2Int(string Base)
        {
            int Int = 0;
            for (int i = 0; i < Base.Length; i++)
            {
                int Index = StrBase.IndexOf(Base[i]);
                if (Index < 0)
                    return -1;
                Int *= LenBase;
                Int += Index;
            }
            return Int;
        }

    }

}
