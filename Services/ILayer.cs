﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Linker.Services
{
    public interface ILayer
    {
        void GetCookie(System.Web.HttpRequestBase Request, System.Web.HttpResponseBase Response);
        string GetUsername();
    }
}