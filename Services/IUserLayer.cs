﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Linker.Services
{
    public interface IUserLayer : ILayer
    {
        void OnLogOut(System.Web.HttpResponseBase Response, System.Web.HttpSessionStateBase Session);
        string OnRegister(System.Web.HttpRequestBase Request, System.Web.HttpResponseBase Response);
        string OnSignIn(System.Web.HttpRequestBase Request, System.Web.HttpResponseBase Response);
    }
}