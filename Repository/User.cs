﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Linker.Repository
{
    class User
    {
        //Key of table
        public int UserID { get; set; }
        public string UserName { get; set; }
        public String Password { get; set; }
        //Using lastCookie
        public string LastCookie { get; set; }
    }
}