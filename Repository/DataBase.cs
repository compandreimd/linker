﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linker.Repository
{
    public class DataBase : IDataBase
    {
        private LocalModel DB;

        public DataBase()
        {
            DB = new LocalModel();
        }

        public DataBase(bool Recreate)
        {
            if (Recreate == true)
            {
                DB = new LocalModel();
                DB.Database.Delete();
                DB.Database.Create();
            }

        }

        public void AddLink(string LongURL, string By, int Count)
        {
            DB.Links.Add(new Link() {
                LongURL = LongURL,
                By = By,
                Count = Count
            });
            Save();
        }

        public List<Link> GetListById(int LinkId)
        {
            return DB.Links.Where(u => u.LinkId == LinkId).ToList();
        }

        public List<Link> GetListBy(string By)
        {
            return DB.Links.Where(u => u.By == By).ToList();
        }

        public bool ExistUser(string Username)
        {
            if (DB.Users.Where(u => u.UserName == Username).ToList().Count > 0)
                return true;
            else
                return false;
        }

        public void AddUser(string Username, string Passwords, string LastCookie)
        {
            DB.Users.Add(new User() {
                UserName = Username,
                Password = Passwords,
                LastCookie = LastCookie
            });
            DB.SaveChanges();
        }

        public String GetUserByCookie(String Cookie)
        {
            var users = DB.Users.Where(u => u.LastCookie == Cookie).ToList();
            if (users.Count > 0)
            {
                return users[0].UserName;
            }
            return null;
        }

        public String GetUserPassword(string Username)
        {
            var users = DB.Users.Where(u => u.UserName == Username).ToList();
            if (users.Count > 0)
            {
                return users[0].Password;
            }
            return null;
        }

        public String GetUserCookie(string Username)
        {
            var users = DB.Users.Where(u => u.UserName == Username).ToList();
            if (users.Count > 0)
            {
                return users[0].LastCookie;
            }
            return null;
        }

        public void ChangesCookie(String OldCookie, String NewCookie )
        {
            if (OldCookie == null || NewCookie == null)
                return;
            DB.Links.Where(l => l.By == OldCookie).ToList().ForEach(l => l.By = NewCookie);
            DB.Users.Where(u => u.LastCookie == OldCookie).ToList().ForEach(u => u.LastCookie = NewCookie);
            DB.SaveChanges();
        }


        public void Save()
        {
            DB.SaveChanges();
        }
    } 
}
