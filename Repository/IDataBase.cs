﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Linker.Repository
{
    public interface IDataBase
    {
        void AddUser(string Username, string Passwords, string LastCookie);
        void ChangesCookie(string OldCookie, string NewCookie);
        bool ExistUser(string Username);
        List<Link> GetListBy(string By);
        List<Link> GetListById(int LinkId);
        string GetUserByCookie(string Cookie);
        string GetUserCookie(string Username);
        string GetUserPassword(string Username);
        void Save();
        void AddLink(string LongURL, string By, int Count);
    }
}