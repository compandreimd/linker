﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Linker.Repository
{
    public class Link
    {
        public Link()
        {

        }
        //Key of table
        [Key]
        public int LinkId { get; set; }
        //Redirect URL
        public String LongURL { get; set; }
        //Actual Saving LastCookie
        public String By { get; set; }
        //Count how many redirects was
        public int Count { get; set; }
    }
}