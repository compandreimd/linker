﻿using System.Data.Entity;

namespace Linker.Repository
{
    class LocalModel : DbContext
    {
        public LocalModel() : base()
        {
            //On update on table
            Database.SetInitializer<LocalModel>(new CreateDatabaseIfNotExists<LocalModel>());
        }
        //Table for links
        public DbSet<Link> Links { get; set; }
        //Table for users
        public DbSet<User> Users { get; set; }
    }
}